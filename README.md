# Ansible

## Installation

sudo yum install python

sudo yum install ansible

sudo yum install java-1.8.0

## Usage

I have install Ansible to run ansible playbook. Using ansible i am installing JIRA software on EC2 instance. 

# Cloudformation

## Template

I have yaml template through which i am provisioning below resources

1. EC2 Instance (t3.large)
2. Security Group to host JIRA
3. RDS instance (db.t2.medium)
4. Ansible playbook inside template will install the JIRA

## How to use template

1. Open Cloudformation Service in AWS console.
2. Click on Create Stack.
3. Now, upload the yaml template
4. Fill the inputs as required ( Enter the Stack Name, DBUserName , DBPassword)
5. Create

## Usage
1. I have used Amazon Linux image to setup the infrastructure.
2. I have provisioned our resources in US East (N. Virginia) us-east-1.
3. A user needs to use the existing Key Pair Name or can generate new key pair
4. This cloudformation template automatically list down the VPC's which are present in the AWS account.
5. I am running a script which updates the DNS name for the IP. I have to add the IP address in the script.

## How to Generate a Key Pair
1. Go to EC2 instance.
2. Select Key Pair from left side pane.
3. Click on Create Key Pair.
4. Give your key a name and download it in PPK (windows) or PEM (other than windows)
